
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def Yossi_Z():
    import string
    ABC = string.ascii_uppercase
    abc = string.ascii_lowercase
    print(ABC[24],abc[14],abc[18],abc[18],abc[8])
    print(ABC[25],abc[0],abc[7],abc[0],abc[21],abc[8])


def Adi_P():
    """
    print my name in random background colors
    :return:
    """
    import random
    name = 'Adi-Peled'
    name_color = ''
    random_colors = []
    for count in range(len(name)):
        random_colors.append(random.randint(41,47))

    for bg, letter in zip(random_colors, name):
        name_color += '\x1b[%sm %s \x1b[0m' % (bg, letter)
    print(name_color)


def Yoav_L():
    pass

def Jonathan_A():
    pass


def Shirly_E():
    pass


def Nadav_A():
    pass


def Uri_Birman():
    pass


def Noam_B_Z():
    pass


def Yuval_B():
    pass


def Stav_D():
    pass


def Omer_D():
    pass


def Geva_H():
    pass


def Yuval_H():
    pass


def Amit_L():
    pass


def Hila_N():
    pass


def Snir_N():
    pass


def Amir_S():
    pass


def Guy_S():
    pass


def Guy_A():
    pass


def Shira_P():
    pass


def Guy_P():
    pass


def Nadav_P():
    pass


def Peleg_K():
    pass


def Tomer_S():
    pass


def Yoav_S():
    pass


def seperator():
    print '--------------------'

if __name__ == '__main__':
    seperator()
    Yossi_Z()
    seperator()
    Adi_P()
    seperator()
    Yoav_L()
    seperator()
    Jonathan_A()
    seperator()
    Shirly_E()
    seperator()
    Nadav_A()
    seperator()
    Uri_Birman()
    seperator()
    Noam_B_Z()
    seperator()
    Yuval_B()
    seperator()
    Stav_D()
    seperator()
    Omer_D()
    seperator()
    Geva_H()
    seperator()
    Yuval_H()
    seperator()
    Amit_L()
    seperator()
    Hila_N()
    seperator()
    Snir_N()
    seperator()
    Amir_S()
    seperator()
    Guy_S()
    seperator()
    Guy_A()
    seperator()
    Shira_P()
    seperator()
    Guy_P()
    seperator()
    Nadav_P()
    seperator()
    Peleg_K()
    seperator()
    Tomer_S()
    seperator()
    Yoav_S()
